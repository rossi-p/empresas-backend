/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var bcrypt = require('bcryptjs')
var jwt = require('jsonwebtoken')
var key = null
var client = null
var uid = null

module.exports = {
    criar: async function(req, res) {
        var email = req.param('email');
        var password = req.param('password');

        var user = await User.findOne({email: email});
        if(user) return res.badRequest('Email já cadastrado!');
  
        var hash = bcrypt.hashSync(password, 10);

        await User.create({ email: email, password: hash }).exec(function (err, result) {
            if (err) return res.serverError(err)
            return res.send({
                success: true
            });
        })

    },
    
	login: async function(req, res) {
   
        var email = req.param('email');
        var user = await User.findOne({email: email});
        
        sails.config.username = email;

       
        if (!user) return res.send({
            success: false,
            errors: "Invalid login credentials. Please try again."
        });

  
        var compare = bcrypt.compareSync(req.param('password'), user.password);
        if(!compare) return res.send({
            success: false,
            errors: "Invalid login credentials. Please try again."
        });
		
        
        token = jwt.sign({user: user.id}, 'segredo', {expiresIn: 160*80});
        client = jwt.sign({user: user.id + 2}, 'segredo', {expiresIn: 160*80});
        uid = user.email;
        res.setHeader('access-token', token);
        res.setHeader('uid', sails.config.username);
        res.setHeader('client', client);
		res.send({
            investor: user,
            success: true
        });
	},

}