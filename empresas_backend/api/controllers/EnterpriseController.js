/**
 * EnterpriseController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    buscaID: async function(req, res) {
        var objeto = {};
        objeto = req;

        if(objeto.params['id']){
            var enterprise = await Enterprise.findOne({id: req.params['id']});
            if(typeof enterprise === 'undefined') return res.badRequest({
                'status': "404",
                "error" : " Not Foud"
        });
        return res.send({
            enterprise: enterprise,
            success: true
        });
        }else{
            if(objeto.param('enterprise_types') && objeto.param('name')){
                var enterprise = await Enterprise.find({
                    where: {name: {contains: objeto.param('name')}, 
                    enterprise_types: objeto.param('enterprise_types')}
                  });
                  return res.send({
                    enterprise: enterprise,
                    success: true
                });
            }else{
                if(objeto.param('enterprise_types')){
                    var enterprise = await Enterprise.find({
                        where: {enterprise_types: objeto.param('enterprise_types')}
                      });
                      return res.send({
                        enterprise: enterprise,
                        success: true
                    });
                }
                if(objeto.param('name')){
                    var enterprise = await Enterprise.find({
                        where: {name: {contains: objeto.param('name')}}
                      });
                      return res.send({
                        enterprise: enterprise,
                        success: true
                    });
                }
                if(!objeto.param('enterprise_types') && !objeto.param('name')){
                    Enterprise.find()
  		                    .then(function(enterprises){
  			                if(!enterprises || enterprises.length === 0){
  				                return res.send({
                                'status': "404",
                                "error" : " Not Foud"
  				                })
  			                }
  			                return res.send({
  				                'data': enterprises
  			                    })
  		                    })
                    }
                }
            }

        
        },

};

