/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'users',

  attributes: {

  password: {
      type: 'STRING',
      required: true
  },
  investor_name: {
      type: 'STRING',
      required: false
  },
  email: {
    type: 'STRING',
    required: true,
    unique: true
 },
  city: {
      type: 'STRING',
      required: false
  },
  country: {
      type: 'STRING',
      required: false
  },
  balance: {
      type: 'FLOAT',
      required: false
  },
  photo: {
      type: 'STRING',
      required: false
  },
  portfolio: {
      type: 'FLOAT',
      required: false
  },
  portfolio_value: {
    type: 'FLOAT',
    required: false
},
  first_access: {
      type: 'BOOLEAN',
      required: false
  },
  super_angel: {
      type: 'BOOLEAN',
      required: false
  },
  enterprise: {
      type: 'STRING',
      required: false
  },
  is_deleted: {
      type: 'BOOLEAN',
      defaultsTo: false
  }
    
  },

  customToJSON() {
		// não retornar a senha
		return _.omit(this, [
      'password',
      'createdAt',
      'updatedAt'
		])
	},

};

