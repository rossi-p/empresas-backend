/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'enterprises',

  attributes: {

    email_enterprise: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    facebook: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    twitter: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    linkedin: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    phone: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    own_enterprise: {
      type: 'BOOLEAN',
      required: false,
      allowNull: true
    },
    enterprise_name: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    photo: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    description: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    city: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    country: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    value: {
      type: 'INTEGER',
      required: false,
      allowNull: true
    },
    share_price: {
      type: 'FLOAT',
      required: false,
      allowNull: true
    },
    name: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    enterprise_types: {
      type: 'STRING',
      required: false,
      allowNull: true
    }
  
  }


};
