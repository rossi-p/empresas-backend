/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to require an authenticated user, or else redirect to login page
 *                 Looks for an Authorization header bearing a valid JWT token
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
var jwt = require('jsonwebtoken')

module.exports = function (req, res, next) {

    var token = req.headers['access-token'];
    const email = req.headers['uid'];
	const client = req.headers['client'];
	
	try {
	var decodedToken = jwt.verify(token, 'segredo',);
	  } catch(err) {
		return res.send({ 
			success: false,
			errors: 'Authorized users only.'});
	  }
	  try {
		var decodedClient = jwt.verify(token, 'segredo',);
		  } catch(err) {
			return res.send({ 
				success: false,
				errors: 'Authorized users only.'});
		  }
	if(decodedToken && decodedClient && sails.config.username === email){
		next();
	}else{
		return res.send({ success: false, errors: 'Authorized users only.' });
	}

};