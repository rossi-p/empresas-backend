/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
    //rota criar usuário para testar
    'POST /api/v1/users/auth/criar_usuario': 'UserController.criar',
    //****************************** */
    'POST /api/v1/users/auth/sign_in': 'UserController.login',
    'GET /api/v1/enterprises/:id': 'EnterpriseController.buscaID',
    'GET /api/v1/enterprises/': 'EnterpriseController.buscaID',

};
